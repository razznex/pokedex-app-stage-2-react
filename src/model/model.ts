// Add all interfaces into this directory
export interface Pokemon {
  name: string;
  id: number;
  sprites: {
    front_default: string;
  };
  caught: boolean;
  types: [
    {
      type: {
        name: string;
      };
    },
  ];
  abilities: [
    {
      ability: {
        name: string;
      };
    },
  ];
  stats: [
    {
      base_stat: number;
      stat: {
        name: string;
      };
    },
  ];
  dateCaught?: string;
}

export type PokemonType =
  | 'bug'
  | 'dark'
  | 'dragon'
  | 'electric'
  | 'fairy'
  | 'fighting'
  | 'fire'
  | 'flying'
  | 'ghost'
  | 'grass'
  | 'ground'
  | 'ice'
  | 'normal'
  | 'poison'
  | 'psychic'
  | 'rock'
  | 'steel'
  | 'water';
