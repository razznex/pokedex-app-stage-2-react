import React, { useEffect, useState } from 'react';
import styles from './Pokedex.module.scss';
import { Pokemon, PokemonType } from '@/model/model';
import { pokemonService } from '@/service/service';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Preloader from '@/view/Preloader/Preloader';
import { typeIcons } from '../../../common/icons/types/PokemonTypes';
import PokemonStatTable from '@/view/PokemonStatTable/PokemonStatTable';
import PokemonInfo from '@/view/Pokedex/PokemonInfo/PokemonInfo';
import { POKEMON_NUMBER } from '../../../common/constants/constants';

const Pokedex = () => {
  const [pokemon, setPokemon] = useState<Pokemon>();
  const [loading, setLoading] = useState(false);
  let { id } = useParams();
  const navigate = useNavigate();
  let idPokemon = Number(id);
  const isValidId = (idPokemon: number) => {
    if (!Number.isNaN(idPokemon)) {
      if (idPokemon > 0 && idPokemon <= POKEMON_NUMBER) return true;
    } else {
      return false;
    }
  };

  useEffect(() => {
    if (!isValidId(idPokemon)) {
      navigate('/not-found');
    } else {
      setLoading(true);
      pokemonService
        .getPokemon(id)
        .then((data) => {
          setPokemon(data);
          setLoading(false);
        })
        .finally(() => setLoading(false));
    }
  }, []);

  return (
    <>
      {loading ? (
        <Preloader />
      ) : (
        <>
          <div className={styles.pokedex}>
            <div className={styles.pokedex__top}>
              <div className={styles.pokedex__screen}>
                <PokemonInfo pokemon={pokemon} />
              </div>
            </div>
            <div className={styles.pokedex__bottom}>
              <div className={styles.pokedex__control}>
                <div
                  className={`${styles.pokedex__button} ${styles.pokedex__button_big}`}
                >
                  <button
                    className={styles.pokedex__back}
                    onClick={() => navigate(-1)}
                  >
                    Назад
                  </button>
                </div>
                <div
                  className={`${styles.pokedex__button} ${styles.pokedex__button_small}`}
                ></div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default Pokedex;
