import React from 'react';
import { typeIcons } from '../../../../common/icons/types/PokemonTypes';
import { Pokemon, PokemonType } from '@/model/model';
import PokemonStatTable from '@/view/PokemonStatTable/PokemonStatTable';
import styles from './PokemonInfo.module.scss';
import { store } from '@/store/store';

interface PokemonInfoProps {
  pokemon: Pokemon;
}

const PokemonInfo: React.FC<PokemonInfoProps> = ({ pokemon }) => {
  const getDateCaught = (id: number): string | undefined => {
    const pokemon = store.state.pokemons.find((pokemon) => pokemon.id === id);
    return pokemon ? pokemon.dateCaught : undefined;
  };

  return (
    <div className={styles.pokemonInfo}>
      <h2 className={styles.pokemonInfo__title}>
        {pokemon?.name}{' '}
        <span className={styles.pokemonInfo__id}>№{pokemon?.id}</span>
      </h2>
      <div className={styles.pokemonInfo__types}>
        {pokemon?.types.map((typeObj) => {
          const typeName = typeObj.type.name;
          if (typeName in typeIcons) {
            return (
              <div
                key={typeObj.type.name}
                className={styles.pokemonInfo__typesContainer}
              >
                <img
                  src={typeIcons[typeName as PokemonType]}
                  alt={typeName}
                  className={styles.pokemonInfo__type}
                />
                <div className={styles.pokemonInfo__typeTooltip}>
                  {typeName}
                </div>
              </div>
            );
          }
          return null;
        })}
      </div>
      <img
        src={`https://www.pokemon.com/static-assets/content-assets/cms2/img/pokedex/full/${pokemon?.id < 10 ? `00${pokemon?.id}` : pokemon?.id < 100 ? `0${pokemon?.id}` : pokemon?.id}.png`}
        alt={pokemon?.name}
        className={styles.pokemonInfo__image}
      />
      {getDateCaught(pokemon?.id) && (
        <p className={styles.pokemonInfo__caught}>
          Пойман: {getDateCaught(pokemon?.id)}
        </p>
      )}
      <div className={styles.pokemonInfo__container}>
        <p className={styles.pokemonInfo__abilitiesName}>Способности:</p>
        <ul className={styles.pokemonInfo__abilities}>
          {pokemon?.abilities.map((abilityObj) => (
            <li
              key={abilityObj.ability.name}
              className={styles.pokemonInfo__abilitiy}
            >
              {abilityObj.ability.name}
            </li>
          ))}
        </ul>
      </div>
      <div className={styles.pokemonInfo__table}>
        <PokemonStatTable pokemon={pokemon} />
      </div>
    </div>
  );
};

export default PokemonInfo;
