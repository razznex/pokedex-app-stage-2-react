import styles from './Header.module.scss';
import pokedex from '@/../common/icons/icons8-pokedex-80.png';
import React from 'react';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

const Header = () => {
  const location = useLocation();

  return (
    <header
      className={`${location.pathname === '/' || location.pathname === '/catched' || /^\/\d+$/.test(location.pathname) ? styles.header : styles.header_hidden} `}
    >
      <Link to='/' className={styles.header__container}>
        <img src={pokedex} alt='pokedex' className={styles.header__logo} />
        <h1 className={styles.header__title}>Pokédex</h1>
      </Link>
      <nav className={styles.header__nav}>
        <Link
          to='/'
          className={`${styles.header__link} ${location.pathname === '/' ? styles.header__link_active : ''} `}
        >
          Главная
        </Link>
        <Link
          to='/catched'
          className={`${styles.header__link} ${location.pathname === '/catched' ? styles.header__link_active : ''} `}
        >
          Пойманные покемоны
        </Link>
      </nav>
    </header>
  );
};

export default Header;
