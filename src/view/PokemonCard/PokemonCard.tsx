import React from 'react';
import { pokemonService } from '@/service/service';
import { Pokemon } from '@/model/model';
import styles from './PokemonCard.module.scss';
import { useNavigate } from 'react-router-dom';

interface PokemonCardProps {
  pokemon: Pokemon;
  onCatch: (pokemon: Pokemon) => void;
  onNavigate: (id: number) => void;
}

const PokemonCard: React.FC<PokemonCardProps> = ({
  pokemon,
  onCatch,
  onNavigate,
}) => {
  return (
    <article
      key={pokemon.id}
      className={`${styles.card} ${pokemon.caught ? styles.card_caught : ''}`}
      onClick={() => onNavigate(pokemon.id)}
    >
      <img
        className={styles.card__image}
        src={pokemon.sprites.front_default}
        alt={pokemon.name}
      />
      <p className={styles.card__title}>{pokemon.name}</p>
      <p className={styles.card__id}>ID: {pokemon.id}</p>
      <button
        className={styles.card__button}
        disabled={pokemon.caught}
        onClick={(e) => {
          e.stopPropagation();
          onCatch(pokemon);
        }}
      >
        <span className={styles.card__span}>
          {pokemon.caught ? 'Пойман' : 'Поймать!'}
        </span>
      </button>
      {pokemon.caught ? (
        <p className={styles.card__caught}>{pokemon.dateCaught}</p>
      ) : (
        ''
      )}
    </article>
  );
};

export default PokemonCard;
