import React, { useState } from 'react';
import Header from '@/view/Header/Header';
import PokemonList from '@/view/PokemonList/PokemonList';
import styles from './Main.module.scss';
import { Route, Routes } from 'react-router-dom';
import Pokedex from '@/view/Pokedex/Pokedex';
import PageNotFound from '@/view/PageNotFound/PageNotFound';
import { Pokemon } from '@/model/model';
import { pokemonService } from '@/service/service';

const View = () => {
  const [catchedPokemons, setCatchedPokemons] = useState<Pokemon[]>([]);
  const handleCatch = (pokemon: Pokemon) => {
    pokemonService.catchPokemon(pokemon);
    setCatchedPokemons((prevCatched) => [...prevCatched, pokemon]);
  };
  return (
    <>
      <Header />
      <main className={styles.main}>
        <Routes>
          <Route
            path='/'
            element={
              <PokemonList
                catchedPokemons={catchedPokemons}
                onCatch={handleCatch}
              />
            }
          />
          <Route
            path='/catched'
            element={
              <PokemonList
                catchedPokemons={catchedPokemons}
                onCatch={handleCatch}
              />
            }
          />
          <Route path='/:id' element={<Pokedex />} />
          <Route path='*' element={<PageNotFound />} />
          <Route path='/not-found' element={<PageNotFound />} />
        </Routes>
      </main>
    </>
  );
};

export default View;
