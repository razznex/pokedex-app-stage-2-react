import React, { useEffect, useState } from 'react';
import { store } from '@/store/store';
import PokemonCard from '@/view/PokemonCard/PokemonCard';
import styles from './PokemonList.module.scss';
import { Pokemon } from '@/model/model';
import { pokemonService } from '@/service/service';
import Preloader from '@/view/Preloader/Preloader';
import { useLocation, useNavigate } from 'react-router-dom';
import professor from '../../../common/icons/professor.png';
import NoPokemon from '@/view/PokemonList/NoPokemon/NoPokemon';
import { POKEMON_NUMBER } from '../../../common/constants/constants';

interface PokemonListProps {
  catchedPokemons: Pokemon[];
  onCatch: (pokemon: Pokemon) => void;
}

const PokemonList: React.FC<PokemonListProps> = ({
  catchedPokemons,
  onCatch,
}) => {
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const [pokemons, setPokemons] = useState<Pokemon[]>(store.state.pokemons);
  const [scrollPosition, setScrollPosition] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(true);
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    if (pokemons.length === 0) {
      pokemonService
        .loadPokemons()
        .then(() => {
          setPokemons([...store.state.pokemons]);
          setLoading(false);
        })
        .finally(() => setLoading(false));
    }
    setLoading(false);
  }, []);

  useEffect(() => {
    if (isFetching) {
      pokemonService.loadPokemons().then(() => {
        setIsFetching(false);
        setPokemons([...store.state.pokemons]);
      });
    }
  }, [isFetching]);

  const handleNavigate = (id: number) => {
    const currentScrollPosition = window.scrollY;
    localStorage.setItem('scrollPosition', currentScrollPosition.toString());
    navigate(`/${id}`);
  };

  const handleScroll = async (e: Event) => {
    const target = e.target as Document;
    setScrollPosition(window.scrollY);
    if (
      target.documentElement.scrollHeight -
        (target.documentElement.scrollTop + window.innerHeight) <
        100 &&
      pokemons.length < POKEMON_NUMBER
    ) {
      if (!isFetching) {
        setIsFetching(true);
      }
    }
  };

  useEffect(() => {
    document.addEventListener('scroll', handleScroll);
    return () => {
      document.removeEventListener('scroll', handleScroll);
    };
  }, [isFetching]);

  window.addEventListener('scroll', () => {
    setScrollPosition(window.scrollY);
  });

  return (
    <>
      {loading ? (
        <Preloader />
      ) : (
        <>
          {location.pathname === '/' ? (
            <section className={styles.pokemon}>
              <div className={styles.pokemon__cards}>
                {pokemons.map((pokemon) => (
                  <PokemonCard
                    key={pokemon.name}
                    pokemon={pokemon}
                    onCatch={onCatch}
                    onNavigate={handleNavigate}
                  />
                ))}
              </div>
              {isFetching ? <Preloader /> : ''}
              <button
                className={
                  scrollPosition < 200
                    ? styles.pokemon__up_hidden
                    : styles.pokemon__up
                }
                onClick={() => {
                  window.scrollTo({ top: 0, behavior: 'smooth' });
                }}
              >
                &#8593;
              </button>
            </section>
          ) : (
            <>
              {catchedPokemons.length !== 0 ? (
                <section className={styles.pokemon}>
                  <div className={styles.pokemon__cards}>
                    {catchedPokemons.map((pokemon) => (
                      <PokemonCard
                        key={pokemon.name}
                        pokemon={pokemon}
                        onCatch={onCatch}
                        onNavigate={handleNavigate}
                      />
                    ))}
                  </div>
                  <button
                    className={
                      scrollPosition < 200
                        ? styles.pokemon__up_hidden
                        : styles.pokemon__up
                    }
                    onClick={() => {
                      window.scrollTo({ top: 0, behavior: 'smooth' });
                    }}
                  >
                    &#8593;
                  </button>
                </section>
              ) : (
                <NoPokemon />
              )}
            </>
          )}
        </>
      )}
    </>
  );
};

export default PokemonList;
