import { Pokemon } from '@/model/model';
import { store } from '@/store/store';
import { pokemonTransport } from '@/transport/transport';
import { formatDateTime } from '../../common/utils/dateTimeFormat';
// import { renderPokemonList } from '@/view/PokemonList/PokemonList';

interface IPokemonService {
  offset: number;
  catchPokemon(pokemon: Pokemon): void;
  loadPokemons(): Promise<void>;
  loadMorePokemons(): Promise<void>;
}

class PokemonService implements IPokemonService {
  offset = 0;

  public catchPokemon(pokemon: Pokemon): void {
    pokemon.caught = true;
    const date = new Date().toISOString();
    pokemon.dateCaught = formatDateTime(date);
    store.updatePokemon(pokemon);
  }

  public async loadPokemons(): Promise<void> {
    try {
      store.setLoading(true);
      const pokemons = await pokemonTransport.fetchPokemonList(this.offset);
      store.addPokemons(pokemons);
      this.offset += 20;
      store.setLoading(false);
    } catch (error) {
      store.setError(error.message);
      store.setLoading(false);
    }
  }

  public async loadMorePokemons(): Promise<void> {
    try {
      const pokemons = await pokemonTransport.fetchPokemonList(this.offset);
      store.addPokemons(pokemons);
      this.offset += 20;
    } catch (error) {
      store.setError(error.message);
    }
  }

  public async getPokemon(id: string | number): Promise<Pokemon> {
    try {
      return await pokemonTransport.fetchPokemon(id);
    } catch (error) {
      store.setError(error.message);
      throw error;
    }
  }
}

export const pokemonService = new PokemonService();
