import { Pokemon } from '@/model/model';
import { configureStore } from '@reduxjs/toolkit';
import pokemonReducer from './reducers/pokemonSlice';

interface Store {
  pokemons: Pokemon[];
  loading: boolean;
  error: string | null;
}

const initialState: Store = {
  pokemons: [],
  loading: false,
  error: null,
};

// Установка первоначальных значений при загрузке страницы
// export const store = {
//   state: { ...initialState },
//
//   setLoading(loading: boolean) {
//     this.state.loading = loading;
//   },
//
//   setError(error: string | null) {
//     this.state.error = error;
//   },
//
//   addPokemons(newPokemons: Pokemon[]) {
//     this.state.pokemons = [...this.state.pokemons, ...newPokemons];
//   },
//
//   updatePokemon(updatedPokemon: Pokemon) {
//     this.state.pokemons = this.state.pokemons.map((pokemon: Pokemon) =>
//       pokemon.id === updatedPokemon.id ? updatedPokemon : pokemon
//     );
//   },
// };

export const setupStore = () => {
  return configureStore({
    reducer: pokemonReducer,
  });
};

export type RootState = ReturnType<typeof pokemonReducer>;
export type AppDispatch = ReturnType<typeof setupStore>;
