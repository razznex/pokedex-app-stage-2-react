import { Pokemon } from '@/model/model';
import { createSlice } from '@reduxjs/toolkit';

interface pokemonStore {
  pokemons: Pokemon[];
  caughtPokemons?: Pokemon[];
  loading: boolean;
  fetching: boolean;
  error: string | null;
}

const initialState: pokemonStore = {
  pokemons: [],
  caughtPokemons: [],
  loading: false,
  fetching: false,
  error: null,
};

export const pokemonSlice = createSlice({
  name: 'pokemon',
  initialState,
  reducers: {},
});

export default pokemonSlice.reducer;
