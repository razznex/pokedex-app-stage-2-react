import '../common/styles/reset/reset.scss';
import '../common/styles/fonts/fonts.scss';
import styles from '@/view/Main.module.scss';
import React from 'react';
import ReactDOM from 'react-dom/client';
import View from '@/view/view';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { setupStore } from '@/store/store';

const root = ReactDOM.createRoot(document.getElementById('root'));
document.getElementById('root').classList.add(styles.root);
const store = setupStore();

root.render(
  <>
    <Provider store={store}>
      <BrowserRouter>
        <View />
      </BrowserRouter>
    </Provider>
  </>
);
